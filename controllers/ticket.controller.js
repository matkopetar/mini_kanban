angular.module('myApp')
  .controller('ticketCtrl', ['$scope', '$http', function($scope, $http) {
  
    // default tickets..
    $scope.tickets = [
      {"id":"1","title":"todo","description":"todo","todo":true,"inprogress":false,"done":false,"remove":function(){var i = $scope.tickets.indexOf(this);$scope.tickets.splice(i, 1);}},
      {"id":"2","title":"inprogress","description":"inprogress","todo":false,"inprogress":true,"done":false,"remove":function(){var i = $scope.tickets.indexOf(this);$scope.tickets.splice(i, 1);}}
    ];

    $scope.counter = 3;
    
    // adding ticket to tickets array
    $scope.addTicket = function () {
      $scope.tickets.push({
        id: $scope.counter,
        title: $scope.title,
        description: $scope.description,
        todo: true,
        inprogress: false,
        done: false,
        remove: function() {
        	var i = $scope.tickets.indexOf(this);
          $scope.tickets.splice(i, 1);
        }
      });
    
      $scope.counter++;
      $scope.title = "";
      $scope.description = "";
    };
    
    //allows dropping
    $scope.allowDrop = function(ev) {
      ev.preventDefault();
    }

    // setting id of dragged item
    $scope.drag = function(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
    }

    // drop to todo column and set todo property to true
    $scope.drop = function(ev, column) {
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      ev.target.appendChild(document.getElementById(data));
      
      $scope.$apply(function() {
        $scope.tickets[data - 1].todo = false;
        $scope.tickets[data - 1].inprogress = false;
        $scope.tickets[data - 1].done = false;
        $scope.tickets[data - 1][column] = true;
      });
    }  

  }]);
angular.module('myApp').directive("ticketDirective", function() {
  return {
    restrict: 'E',
    template: '<div><button class="ticketRemoveButton" ng-click="ticket.remove()">X</button><div class="mdl-textfield mdl-js-textfield"><input class="mdl-textfield__input" type="text" id="title" ng-model="ticket.title" ></div><div class="mdl-textfield mdl-js-textfield"><textarea class="mdl-textfield__input" id="description" ng-model="ticket.description" ></div></div>',
    link: function(scope, element, attrs) {
      element.bind("keyup", function() {
        var el = element[0].querySelector('#description');
        var scrollHeight = el.scrollHeight + 1;
        el.style.height = scrollHeight + "px";
      });
    }
  };
});

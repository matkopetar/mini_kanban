angular.module('myApp').directive("headingDirective", function() {
  return {
    restrict: 'E',
    template: '<div class="col-xs-4"><h5>To Do</h5></div><div class="col-xs-4"><h5>In Progress</h5></div><div class="col-xs-4"><h5>Done</h5></div>'
  };
});
